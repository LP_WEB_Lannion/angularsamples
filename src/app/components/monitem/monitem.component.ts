import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
@Component({
  selector: 'app-monitem',
  templateUrl: './monitem.component.html',
  styleUrls: ['./monitem.component.css']
})
export class MonitemComponent implements OnInit {
  @Input() name:string;
  @Output() nameChange = new EventEmitter<string>();
  constructor() { /*monElement est pas encore instantié */}
  ngOnInit() {console.log(this.name) /*monElement est instantié */}
  onClick()
  {this.nameChange.emit(this.name + '*');}
}
