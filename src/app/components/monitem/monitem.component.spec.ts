import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitemComponent } from './monitem.component';

describe('MonitemComponent', () => {
  let component: MonitemComponent;
  let fixture: ComponentFixture<MonitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
