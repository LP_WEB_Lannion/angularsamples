import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Router, NavigationExtras} from "@angular/router";
@Component({
  selector: 'app-myform',
  templateUrl: './myform.component.html',
  styleUrls: ['./myform.component.css']
})
export class MyformComponent implements OnInit {
  model:{nom:string,mail:string,genre:string};
  genres:Array<string>=["homme","femme","autre"]
  constructor(private route: ActivatedRoute, private router: Router) {
   this.model={nom:"john",mail:"jd@toto.com",genre:this.genres[0]};
   }
   submitsimple() {
     alert(JSON.stringify(this.model))
     let navigationExtras: NavigationExtras = {
      queryParams:this.model
      };
     
      
     this.router.navigate(["/morceau"], navigationExtras);
     
  }
  ngOnInit() {

    this.route.data
    .subscribe((data) => {
    alert(data.title);
    });

  }
}
