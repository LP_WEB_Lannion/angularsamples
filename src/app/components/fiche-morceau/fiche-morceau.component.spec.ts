import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheMorceauComponent } from './fiche-morceau.component';

describe('FicheMorceauComponent', () => {
  let component: FicheMorceauComponent;
  let fixture: ComponentFixture<FicheMorceauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheMorceauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheMorceauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
