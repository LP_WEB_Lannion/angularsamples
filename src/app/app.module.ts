import { DeezerService } from './services/deezer.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FicheMorceauComponent } from './components/fiche-morceau/fiche-morceau.component';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './directive/highlight.directive';
import { PowtwoPipe } from './pipes/powtwo.pipe';
import { MonitemComponent } from './components/monitem/monitem.component';
import { SearchComponent } from './components/search/search.component';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MyformComponent } from './components/myform/myform.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const appRoutes: Routes = [
  { path: 'morceau',      component: FicheMorceauComponent },
  { path: '',
    redirectTo: '/form',
    pathMatch: 'full'
  },
  {
    path: 'form',
    component: MyformComponent,
    data: { title: 'Login' }
  },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    FicheMorceauComponent,
    HighlightDirective,
    PowtwoPipe,
    MonitemComponent,
    SearchComponent,
    MyformComponent,
    PageNotFoundComponent,

  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    DeezerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
