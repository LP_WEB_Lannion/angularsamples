import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {
  @Input()
  appHighlight?: string;
  constructor(private el: ElementRef) {}

  ngOnInit() {
    if (this.appHighlight) this.el.nativeElement.style.backgroundColor = this.appHighlight;
    else this.el.nativeElement.style.backgroundColor = 'yellow';
  }
}
